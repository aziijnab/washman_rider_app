import React from "react";
import {
  createStackNavigator,
  createAppContainer,
  StackNavigator
} from "react-navigation";
import LoginForm from "./screens/LoginScreens/LoginForm";
import TodaysPickups from "./screens/LoginScreens/UserProfile/home_tabs/TodaysPickups";
import TodaysDelivery from "./screens/LoginScreens/UserProfile/home_tabs/TodaysDelivery";
import HomePage from "./screens/LoginScreens/UserProfile/home_tabs/HomePage";
import OrderDetails from "./screens/LoginScreens/UserProfile/home_tabs/OrderDetails";
import DetailsForRider from "./screens/LoginScreens/UserProfile/home_tabs/DetailsForRider";
import Pickup from "./screens/LoginScreens/UserProfile/home_tabs/Pickup";
import Deliver from "./screens/LoginScreens/UserProfile/home_tabs/Deliver";
const TopLevelLoginNavigator = createStackNavigator(
  {
    HomePage: { screen: HomePage },
    TodaysPickups: { screen: TodaysPickups },
    TodaysDelivery: { screen: TodaysDelivery },
    Pickup: { screen: Pickup },
    Deliver: { screen: Deliver },
    OrderDetails: { screen: OrderDetails },
    DetailsForRider: { screen: DetailsForRider },

    LoginForm: { screen: LoginForm },
  },
  {
    headerMode: "none"
  }
  );

  const TopLevelLogin = createAppContainer(TopLevelLoginNavigator);
export default TopLevelLogin;
