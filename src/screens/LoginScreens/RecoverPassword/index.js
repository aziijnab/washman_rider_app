//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity, Alert,
  ActivityIndicator
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Container } from "native-base";
import AccountHeader from "../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";
import { URL, TOKEN } from "../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class RecoverPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: "",
      confirmPassword: "",
      newPassword_err: "",
      confirmPassword_err: "",
      isLoading: false,
    };
    this.data = this.props.navigation.state.params;
  }
  submit = () => {
    const { newPassword , confirmPassword} = this.state;
    
    if (newPassword == "") {
      this.setState({ newPassword_err: "Required" });
    }
    else{
      this.setState({ newPassword_err: "" });
    }
    if (confirmPassword != newPassword) {
      this.setState({ confirmPassword_err: "Password doesn't match" });
    }
    else{
      this.setState({ confirmPassword_err: "" });
    }
 
    if(newPassword != "" && confirmPassword == newPassword){
      this.setState({ isLoading: true });
      fetch(URL + "Customer/ChangePassword", {
        method: "PUT",
        headers: {
         AuthToken:TOKEN,
         newPassword: newPassword,
         Username: this.data.username,
        }
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.ResponseCode == 200) {
            this.setState({ isLoading: false });
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }


        })
        .catch(error => alert("Please Check Your Internet Connection"));
    }
  };
  render() {
    const { navigation } = this.props.navigation;
    return (

      <View style={{ height: deviceHeight, width: deviceWidth }}>
       <AccountHeader navigation={this.props.navigation} back={true} />

       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:50}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_regular",textAlign:'center' }}>
               "Enter your New Password"
              </Text>
             </View>
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingBottom: 100,
                paddingTop:'10%'
              }}
            >
              <TextField
                autoCapitalize="none"
                error={this.state.newPassword_err}
                onChangeText={newPassword => this.setState({ newPassword })}
                autoCorrect={false}
                label="New Password"
                maxLength={20}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
              <TextField
                autoCapitalize="none"
                error={this.state.confirmPassword_err}
                onChangeText={confirmPassword => this.setState({ confirmPassword })}
                autoCorrect={false}
                label="Confirm Password"
                maxLength={20}
                tintColor={"#000"}
                baseColor="#000"
                textColor="#000"
              />
             
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop : 25
                }}
              >
              {this.state.isLoading ? (
                <Button
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: deviceHeight/5,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <ActivityIndicator size="small"/>
                </Button>
              ):(
                <Button
                   onPress={this.submit}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: (deviceWidth * 2) / 3,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: deviceHeight/5,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    SEND
                  </Text>
                </Button>
              )}
              </View>
              </View>

            {/* ............Button................. */}
          </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default RecoverPassword;
