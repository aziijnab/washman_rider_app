import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import { Button } from "native-base";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
class First extends React.Component {
 
  render() {
    //const { navigation } = this.props.navigation;
    const navigation = this.props
    return (
        <View style={{position:'absolute'}}>
                             <View 
                             style={{
                                 alignItems: "center",
                                 justifyContent: "center"}}>
                <Button
                  bordered
                  heightPercentageToDP={70}
                  onPress = {() => this.props.navigation.navigate('FTL_cargoDescription') }
                  style={{
                    width: (deviceWidth * 2) / 2.3,
                  // height: (deviceHeight * 1) / 4 - 100,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#3B5998",
                    backgroundColor: "#fff",
                    top:"25%",
                    marginHorizontal:"6%"
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#111",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    FULL TRUCK LOAD (FTL)
                  </Text>
                </Button>
                <Button
                  bordered
                  heightPercentageToDP={70}
                  style={{
                    width: (deviceWidth * 2) / 2.3,
                  // height: (deviceHeight * 1) / 4 - 100,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#3B5998",
                    backgroundColor: "#fff",
                    top:"30%",
                    marginHorizontal:"6%"
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#111",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    LTC OR LTL
                  </Text>
                </Button>
                <Button
                  bordered
                  heightPercentageToDP={70}
                  style={{
                    width: (deviceWidth * 2) / 2.3,
                  // height: (deviceHeight * 1) / 4 - 100,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#3B5998",
                    backgroundColor: "#fff",
                    top:"35%",
                    marginHorizontal:"6%"
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#111",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    FULL CONTAINER LOAD (FCL)
                  </Text>
                </Button>
                             </View>
                           
                           </View>
      );
  }
}
export default First;