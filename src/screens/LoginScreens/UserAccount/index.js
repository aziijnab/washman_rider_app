import React, { Component } from 'react';
import { Platform, StyleSheet, Text, 
    View, FlatList, TouchableOpacity, 
    Image, SafeAreaView, Dimensions } from 'react-native';
    import { LinearGradient } from "expo";
    import First from "./first";
import ic_menu from './Image/list.png'
import dashboard_menu1 from './Image/delivery-truck.png'
import Drawer from 'react-native-drawer'
import { Ionicons } from '@expo/vector-icons';
console.disableYellowBox = true;


var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const menu = [
    
    { 'title': 'Your Orders', 'icon': 'car'},
    { 'title': 'Settings', 'icon': 'md-notifications' },
    { 'title': 'Notifications', 'icon': 'car' },
    { 'title': 'Contact us', 'icon': 'car' }
]

class UserAccount extends Component {

    constructor(props) {
        super(props)

       
    }

    renderDrawer() {
        //SlideMenu
        return (
            <View style={styles.menuContainer}>
             <LinearGradient
            colors={["#ff9966", "#ff5e62"]}
            style={styles.drawerMenuHeader}
          >
  <View style={{marginVertical:35}}>
<Image source={{ uri:"https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png" }} style={styles.profileImg} />
</View>
<View style={{marginVertical:10}}>
  <Text>Packages Limited</Text>
</View>
          </LinearGradient>
                <FlatList
                    style={{ flex: 1.0 }}
                    data={menu}
                    extraData={this.state}
                    renderItem={({ item, index }) => {
                        return (
                        <TouchableOpacity style={styles.menuTitleContainer}>
                            
                            <Image style={{marginHorizontal:"5%"}} source={dashboard_menu1}/>
                            <Ionicons
                              style={{ marginHorizontal:"5%"}}
                              name={item.icon}
                              size={24}
                              color="#9EA0A4"></Ionicons>
                                <Text style={styles.menuTitle}
                                    key={index}>
                                    {item.title}
                                </Text>
                            </TouchableOpacity>
                        )
                    }} />
            </View>
        )
    }

    openDrawer() {
        this.drawer.open()
    }

    closeDrawer() {
        this.drawer.close()
    }

    render() {
        const navigation = this.props.navigation
        return (
          
            <SafeAreaView style={styles.safeAreaStyle}>
                <View style={styles.mainContainer}>
                

                    <Drawer
                        ref={(ref) => this.drawer = ref}
                        content={this.renderDrawer()}
                        type='static'
                        tapToClose={true}
                        openDrawerOffset={0.35}
                        styles={drawerStyles}>
                        {/* //Main View */}
                        <View style={styles.headerContainer}>
                            <View style={styles.menuButton}>
                                <TouchableOpacity
                                    onPress={this.openDrawer.bind(this)}>
                                    <Image style={{ tintColor: 'white' }} source={ic_menu} />
                                  
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.headerTitle}>Select Order Type </Text>
                            <View style={styles.menuButton} />
                        </View>
                       <View>
                         <View>
                         
                           <First navigation = {navigation}/>
                         </View>
                       </View>
                    </Drawer>
                    
                </View>
            </SafeAreaView>
        );
    }
}

const drawerStyles = {
    drawer: {
        flex: 1.0,
        backgroundColor: '#3B5998',
        
    },
    main: {
        flex: 1.0,
        backgroundColor: 'white'
    }
}

const styles = {
    mainContainer: {
        flex: 1.0,
        backgroundColor: 'white'
    },
    safeAreaStyle: {
        flex: 1.0,
        backgroundColor: '#3B5998',
    },
    headerContainer: {
        height: 44,
        flexDirection: 'row',
        justifyContect: 'center',
        backgroundColor: '#3B5998',
    },
    headerTitle: {
        flex: 1.0,
        textAlign: 'center',
        alignSelf: 'center',
        color: 'white'
    },
    menuButton: {
        marginLeft: 8,
        marginRight: 8,
        alignSelf: 'center',
        tintColor: 'white'
    },
    menuContainer: {
        flex: 1.0,
        backgroundColor: '#3B5998',
    },
    menuTitleContainer: {
        height: 60,
        width:'100%',
        flexDirection:'row',
    },
    menuTitle: {
        width:'100%',
        color: 'white',
        fontSize: 17,
        // alignSelf:'left',
        marginTop:"10%",
        marginLeft:"4%"
    }, profileImg: {
      height: 100,
      width: 100,
      borderRadius: 50,
    },
    drawerMenuHeader:{
      flex: 1,
      alignItems: "center",
      borderBottomWidth: 2,
      
    },
    map:{
   height:(deviceHeight * 2) / 2,
    }
}
export default UserAccount;
