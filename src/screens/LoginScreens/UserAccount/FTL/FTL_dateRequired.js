import React, { Component } from 'react';
import { Platform, StyleSheet, Text, 
    View, FlatList, TouchableOpacity, 
    Image, SafeAreaView, Dimensions } from 'react-native';
import DatePicker from 'react-native-datepicker'
    import { Button } from "native-base";
    import { LinearGradient } from "expo";
import ic_menu from '../Image/list.png'
import Drawer from 'react-native-drawer'
console.disableYellowBox = true;

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const menu = [
    { 'title': 'Your Orders'},
    { 'title': 'Settings'},
    { 'title': 'Notifications' },
    { 'title': 'Contact us' }
]

class FTL_dateRequired extends Component {


    constructor(props) {
        super(props)

        this.state = {
           
            chosenDate: new Date(),
            modalVisible: false,
            date_required:"2016-05-15",
            date_delivery:"2016-05-15",
           
          };
          
    }
 
    renderDrawer() {
        //SlideMenu
      
        return (
            <View style={styles.menuContainer}>
             <LinearGradient
            colors={["#ff9966", "#ff5e62"]}
            style={styles.drawerMenuHeader}
          >
  <View style={{marginVertical:35}}>
<Image source={{ uri:"https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png" }} style={styles.profileImg} />
</View>
<View style={{marginVertical:10}}>
  <Text style={{color:'#fff', fontSize:22}}>Packages Limited</Text>
</View>
          </LinearGradient>
                <FlatList
                    style={{ flex: 1.0 }}
                    data={menu}
                    extraData={this.state}
                    renderItem={({ item, index }) => {
                        return (
                        <TouchableOpacity style={styles.menuTitleContainer}>
                                <Text style={styles.menuTitle}
                                    key={index}>
                                    {item.title}
                                </Text>
                            </TouchableOpacity>
                        )
                    }} />
            </View>
        )
    }

    openDrawer() {
        this.drawer.open()
    }

    closeDrawer() {
        this.drawer.close()
    }
    
    render() {
        const navigation = this.props.navigation
       
      
        return (
          
            <SafeAreaView style={styles.safeAreaStyle}>
                <View style={styles.mainContainer}>
                

                    <Drawer
                        ref={(ref) => this.drawer = ref}
                        content={this.renderDrawer()}
                        type='static'
                        tapToClose={true}
                        openDrawerOffset={0.35}
                        styles={drawerStyles}>
                        {/* //Main View */}
                        <View style={styles.headerContainer}>
                            <View style={styles.menuButton}>
                                <TouchableOpacity
                                    onPress={this.openDrawer.bind(this)}>
                                    <Image style={{ tintColor: 'white' }} source={ic_menu} />
                                  
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.headerTitle}>FTL - Schedule Shipment</Text>
                            <View style={styles.menuButton} />
                        </View>
                       <View  style={styles.mainbody}>
                           
                           <View style={{position:'absolute'}}>
                           <View style={{flex:1, justifyContent:'center', alignItems:'center' , flexDirection:'column', paddingTop:'30%'}}>
     <Text style={{fontSize:18,fontWeight:'bold', marginVertical:"4%"}}>Required Date and Time</Text>
        <DatePicker
      datePicker={true}
        style={{width: "80%"}}
        date={this.state.date_required}
        androidMode={'spinner'}
        mode="datetime"
        is24Hour={false}
        placeholder="select date"
        format="YYYY-MM-DD, h:mm:ss a"
        minDate="2016-05-01"
        maxDate="2016-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date_required: date})}}
      />
      </View>
      <View style={{flex:1, justifyContent:'center' , flexDirection:'column'}}>
        <View style={{marginTop:"20%", flex:1,flexDirection:'column',width:deviceWidth,justifyContent:'center', alignItems:'center'}} >
        <Text style={{fontSize:18,fontWeight:'bold', marginVertical:"4%"}}>Delivery Date and Time</Text>
        <DatePicker
      datePicker={true}
        style={{width: "80%"}}
        date={this.state.date_delivery}
        androidMode={'spinner'}
        mode="datetime"
        is24Hour={false}
        placeholder="select date"
        format="YYYY-MM-DD, h:mm:ss a"
        minDate="2016-05-01"
        maxDate="2016-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date_delivery: date})}}
      />

        </View>
      
      </View>
      </View>
      <View style={{position:'absolute', bottom:0}}>
      <View style={{flex:1, justifyContent:'center',alignContent:'center',height:50}}>
     <View style={{flex:1,flexDirection:'row', width:deviceWidth,justifyContent:'center'}}>
     <Button
                  bordered
                  heightPercentageToDP={50}
                  onPress = {() => this.props.navigation.navigate('FTL_SelectTruck') }
                  style={{
                    width: (deviceWidth * 2) / 4.2,
                  // height: (deviceHeight * 1.2) / 4 - 120,
                  alignSelf:'flex-end',
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#000",
                    backgroundColor: "#9EA0A4",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    Back
                  </Text>
                </Button>
                <Button
                  bordered
                  heightPercentageToDP={50}
                  onPress = {() => this.props.navigation.navigate('FTL_selectLocations') }
                  style={{
                    width: (deviceWidth * 2) / 4.2,
                  // height: (deviceHeight * 1.2) / 4 - 120,
                  alignSelf:'flex-end',
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#000",
                    backgroundColor: "#1c7619",
                    marginLeft:3,
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    Next
                  </Text>
                </Button>

     </View>
                
                    
      </View>
      </View>
                           
                            
                         </View>
                       
                    </Drawer>
                    
                </View>
            </SafeAreaView>
        );
    }
}

const drawerStyles = {
    drawer: {
        flex: 1.0,
        backgroundColor: '#3B5998',
        
    },
    main: {
        flex: 1.0,
        backgroundColor: 'white'
    }
}
const smallpickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: 16,
      paddingVertical: 8,
      paddingHorizontal: 7,
      borderWidth: 1,
      borderColor: '#9EA0A4',
      borderRadius: 4,
      color: 'black',
      paddingRight: 30, 
      backgroundColor:'#fff',
      width: (deviceWidth * 2) / 2.3,
      // to ensure the text is never behind the icon
    },
    inputAndroid: {
      fontSize: 16,
      paddingHorizontal: 10,
      paddingVertical: 8,
      borderWidth: 1,
      borderColor: '#9EA0A4',
      borderRadius: 4,
      color: 'black',
      paddingRight: 30, 
      backgroundColor:'#fff',
      width: (deviceWidth * 2) / 2.3,
    },
  });
const styles = {
    mainbody:{
flex:1,
justifyContect:'center',
    },
    mainContainer: {
        flex: 1.0,
        backgroundColor: 'white'
    },
    safeAreaStyle: {
        flex: 1.0,
        backgroundColor: '#3B5998',
    },
    headerContainer: {
        height: 44,
        flexDirection: 'row',
        justifyContect: 'center',
        backgroundColor: '#3B5998',
    },
    headerTitle: {
        flex: 1.0,
        textAlign: 'center',
        alignSelf: 'center',
        color: 'white'
    },
    menuButton: {
        marginLeft: 8,
        marginRight: 8,
        alignSelf: 'center',
        tintColor: 'white'
    },
    menuContainer: {
        flex: 1.0,
        backgroundColor: '#3B5998',
    },
    menuTitleContainer: {
        alignItem:'center',
        height: 60,
        width:'100%',
        flexDirection:'row',
    },
    menuTitle: {
        width:'100%',
        color: 'white',
        textAlign: 'center',
        fontSize: 17,
        alignSelf:'center',
    }, profileImg: {
      height: 100,
      width: 100,
      borderRadius: 50,
    },
    drawerMenuHeader:{
      flex: 1,
      alignItems: "center",
      borderBottomWidth: 2,
      
    },
    map:{
   height:(deviceHeight * 2) / 2,
    }
}
export default FTL_dateRequired;
