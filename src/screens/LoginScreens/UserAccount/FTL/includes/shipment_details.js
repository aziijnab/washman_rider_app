import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions,TextInput,CheckBox } from "react-native";

import { Button } from "native-base";
import RNPickerSelect from 'react-native-picker-select';
import { Ionicons } from '@expo/vector-icons';
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

const Loads = [
  {
    label: 'Bags',
    value: 'bags',
  },
  {
    label: 'Bundles',
    value: 'bundles',
  },
  {
    label: 'Coils',
    value: 'coils',
  },
];
const weights = [
  {
    label: 'Tons',
    value: 'tons',
  },
  {
    label: 'Kgs',
    value: 'kgs',
  },
  {
    label: 'Lbs',
    value: 'lbs',
  },
  {
    label: 'Cbm',
    value: 'cbm',
  },
];
class Shipment_Details extends React.Component {
  constructor(props) {
    super(props);


    this.state = {
      selected_truck: 'Select a sport...',
      selected_loadType: 'Select load type...',
      selected_direction: 'Select Direction...',
      selected_estimatedWeight: 'Select Estimated Weight...',
      checked: false
    };
  }
  onChangeCheck() {
    this.setState({ checked: !this.state.checked})
}
  render() {
   
    const placeholder_loadType = {
      label: 'Type of Load...',
      value: null,
      color: '#9EA0A4',
    };
    const placeholder_direction = {
      label: 'Select Direction...',
      value: null,
      color: '#9EA0A4',
    };
    const placeholder_estimatedWeight = {
      label: 'Weight ...',
      value: null,
      color: '#9EA0A4',
    };
    const navigation = this.props
   
    return (
        <View style={{position:'absolute' }}>
                             
                               <View 
                             style={{
                                 paddingVertical: 3,
                                 paddingHorizontal:15,
                                //  borderColor:"#000",
                                //  borderWidth:1,
                                //  borderRadius:1,
                                //  backgroundColor:"#fff",
                                 width: (deviceWidth * 2) / 2.1,
                                 marginLeft:'2%',
                                 flex:1}}>

                                 <View style={{paddingVertical:3, flex:1}}>

                                

                                 <RNPickerSelect
          placeholder={placeholder_loadType}
          items={Loads}
          onValueChange={value => {
            this.setState({
              selected_loadType: value,
            });
          }}
          style={{
            ...smallpickerSelectStyles,
            iconContainer: {
              top: 10,
              right: 12,
            },
            width:30,
          }}
          value={this.state.selected_loadType}
          useNativeAndroidPickerStyle={false}
          textInputProps={{ underlineColor: '#3B5998' }}
          Icon={() => {
            return <Ionicons style={{paddingTop:2}} name="md-arrow-down" size={24} color="#9EA0A4" />;
          }}
        /> 
                                 </View>
                                 <View style={{paddingVertical:3}}>
<RNPickerSelect
placeholder={placeholder_direction}
items={Loads}
onValueChange={value => {
this.setState({
selected_direction: value,
});
}}
style={{
...smallpickerSelectStyles,
iconContainer: {
top: 10,
right: 12,
},
}}
value={this.state.selected_direction}
useNativeAndroidPickerStyle={false}
textInputProps={{ underlineColor: '#3B5998' }}
Icon={() => {
return <Ionicons style={{paddingTop:2}} name="md-arrow-down" size={24} color="#9EA0A4" />;
}}
/> 
</View>
 <View style={{paddingVertical:3, 
 flex:1, flexDirection:'row'}}>
 <TextInput
  keyboardType={"numeric"}
    placeholder={'Estimated'}
      style={{width:"30%", borderWidth: 1,
    borderColor: '#9EA0A4',
    borderRadius: 4,
    backgroundColor:'#fff', paddingHorizontal:10}}
    />
<RNPickerSelect
placeholder={placeholder_estimatedWeight}
items={weights}
onValueChange={value => {
this.setState({
selected_estimatedWeight: value,
});
}}
style={{
...weightpickerSelectStyles,
iconContainer: {
top: 10,
right: 12,
},
}}
value={this.state.selected_estimatedWeight}
useNativeAndroidPickerStyle={false}
textInputProps={{ underlineColor: '#3B5998' }}
Icon={() => {
return <Ionicons style={{paddingTop:2}} name="md-arrow-down" size={24} color="#9EA0A4" />;
}}
/> 
</View>
<View style={input_description.textarea}>
<TextInput
    multiline={true}
    placeholder={'Enter Shipment Description'}
    numberOfLines={4}
    onChangeText={(text) => this.setState({text})}
    value={this.state.text}
      style={{height:(deviceHeight * 2) / 14,
    justifyContent: "flex-start"}}
    />
</View>
<View style={{backgroundColor:"#fff", paddingVertical:10, marginTop: 5}}>
<View style={{flex: 1, flexDirection: 'row',paddingHorizontal:10,paddingVertical:5}}>
<View style={{flex:2, flexDirection: 'row'}}>
<CheckBox style={{width:20,height:20,marginTop:5}} value={this.state.checked} onChange={() => this.onChangeCheck()} />
<Text style={{paddingTop:5,paddingLeft:10}}>Loading</Text>
</View>

<View style={{flex:2, flexDirection: 'row'}}>
<CheckBox style={{width:20,height:20,marginTop:5}} value={true} />
<Text style={{paddingTop:5,paddingLeft:10, color: 'red'}}>Crane Needed</Text>
</View>
      </View>
      <View style={{flex: 1, flexDirection: 'row',paddingHorizontal:10,paddingVertical:5}}>
<View style={{flex:2, flexDirection: 'row'}}>
<CheckBox style={{width:20,height:20,marginTop:5}} value={this.state.checked} onChange={() => this.onChangeCheck()} />
<Text style={{paddingTop:5,paddingLeft:10}}>Unloading</Text>
</View>

<View style={{flex:2, flexDirection: 'row'}}>
<CheckBox style={{width:20,height:20,marginTop:5}} value={true} />
<Text style={{paddingTop:5,paddingLeft:10, color: 'red'}}>Containerized</Text>
</View>
      </View>
      <View style={{flex: 1, flexDirection: 'row',paddingHorizontal:10,paddingVertical:5}}>
<View style={{flex:2, flexDirection: 'row'}}>
<CheckBox style={{width:20,height:20,marginTop:5}} value={this.state.checked} onChange={() => this.onChangeCheck()} />
<Text style={{paddingTop:5,paddingLeft:10  , color: 'red' }}>Heavy Lifting</Text>
</View>

<View style={{flex:2, flexDirection: 'row'}}>
<CheckBox style={{width:20,height:20,marginTop:5}} value={true} />
<Text style={{paddingTop:5,paddingLeft:10,  color: 'red'}}>Labor</Text>
</View>
      </View>
      
</View>
        
                </View>
                <View style={{paddingTop:"2%", flex:1, flexDirection:'row', marginTop:"42%", alignItems:'center', justifyContent:'center'}}>
                <Button
                  bordered
                  onPress = {() => this.props.navigation.navigate('UserAccount') }
                  style={{
                    width: (deviceWidth * 2) / 4.2,
                  height: (deviceHeight * 1.2) / 4 - 120,
                  alignSelf:'flex-end',
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#000",
                    backgroundColor: "#9EA0A4",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    Back
                  </Text>
                </Button>
                <Button
                  bordered
                  onPress = {() => this.props.navigation.navigate('FTL_SelectTruck') }
                  style={{
                    width: (deviceWidth * 2) / 4.2,
                  height: (deviceHeight * 1.2) / 4 - 120,
                  alignSelf:'flex-end',
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#000",
                    backgroundColor: "#1c7619",
                    marginLeft:3,
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 17,
                      fontWeight: "600"
                    }}
                  >
                    Next
                  </Text>
                </Button>
              </View>        
                           </View>
      );
  }
}
const styles = StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 10,
    flex: 1,
  },
});

const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 7,
    borderWidth: 1,
    borderColor: '#9EA0A4',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, 
    backgroundColor:'#fff',
    width: (deviceWidth * 2) / 2.3,
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: '#9EA0A4',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30,
    backgroundColor:'#fff',
    width: (deviceWidth * 2) / 2.3, // to ensure the text is never behind the icon
  },
});
const weightpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 7,
    borderWidth: 1,
    borderColor: '#9EA0A4',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, 
    backgroundColor:'#fff',
    width: "100%",
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingLeft: 20,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: '#9EA0A4',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, 
    backgroundColor:'#fff',
    width: "100%",
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
const input_description = {
  textarea: {
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#9EA0A4',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, 
    backgroundColor:'#fff',
    width: (deviceWidth * 2) / 2.3,
  }
}
export default Shipment_Details;