import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  SafeAreaView,
  Dimensions, TextInput, CheckBox 
} from "react-native";
import { LinearGradient } from "expo";
import Shipment_Details from "./includes/shipment_details";
import ic_menu from "../Image/list.png";
import Drawer from "react-native-drawer";
import { Button } from "native-base";
import RNPickerSelect from "react-native-picker-select";
import { Ionicons } from "@expo/vector-icons";
console.disableYellowBox = true;

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const menu = [
  { title: "Your Orders" },
  { title: "Settings" },
  { title: "Notifications" },
  { title: "Contact us" }
];

const Loads = [
  {
    label: "Bags",
    value: "bags"
  },
  {
    label: "Bundles",
    value: "bundles"
  },
  {
    label: "Coils",
    value: "coils"
  }
];
const weights = [
  {
    label: "Tons",
    value: "tons"
  },
  {
    label: "Kgs",
    value: "kgs"
  },
  {
    label: "Lbs",
    value: "lbs"
  },
  {
    label: "Cbm",
    value: "cbm"
  }
];

class FTL_cargoDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected_truck: "Select a sport...",
      selected_loadType: "Select load type...",
      selected_direction: "Select Direction...",
      selected_estimatedWeight: "Select Estimated Weight...",
      loading: false,
      unloading: false,
      heavyLifting: false,
      craneNeeded: false,
      containerized: false,
      labor: false,
    };
  }
  onChangeLoading() {
    this.setState({ loading: !this.state.loading });
  }
  onChangeunloading() {
    this.setState({ unloading: !this.state.unloading });
  }

  onChangeheavyLifting() {
    this.setState({ heavyLifting: !this.state.heavyLifting });
  }

  onChangeCrane() {
    this.setState({ craneNeeded: !this.state.craneNeeded });
  }

  onChangeContainerized() {
    this.setState({ containerized: !this.state.containerized });
  }

  onChangeLabor() {
    this.setState({ labor: !this.state.labor });
  }


  renderDrawer() {
    //SlideMenu
    return (
      <View style={styles.menuContainer}>
        <LinearGradient
          colors={["#ff9966", "#ff5e62"]}
          style={styles.drawerMenuHeader}
        >
          <View style={{ marginVertical: 35 }}>
            <Image
              source={{
                uri:
                  "https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png"
              }}
              style={styles.profileImg}
            />
          </View>
          <View style={{ marginVertical: 10 }}>
            <Text style={{ color: "#fff", fontSize: 22 }}>
              Packages Limited
            </Text>
          </View>
        </LinearGradient>
        <FlatList
          style={{ flex: 1.0 }}
          data={menu}
          extraData={this.state}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity style={styles.menuTitleContainer}>
                <Text style={styles.menuTitle} key={index}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    );
  }

  openDrawer() {
    this.drawer.open();
  }

  closeDrawer() {
    this.drawer.close();
  }

  render() {
    const placeholder_loadType = {
      label: "Type of Load...",
      value: null,
      color: "#9EA0A4"
    };
    const placeholder_direction = {
      label: "Select Direction...",
      value: null,
      color: "#9EA0A4"
    };
    const placeholder_estimatedWeight = {
      label: "Weight ...",
      value: null,
      color: "#9EA0A4"
    };
    const navigation = this.props.navigation;
    return (
      <SafeAreaView style={styles.safeAreaStyle}>
        <View style={styles.mainContainer}>
          <Drawer
            ref={ref => (this.drawer = ref)}
            content={this.renderDrawer()}
            type="static"
            tapToClose={true}
            openDrawerOffset={0.35}
            styles={drawerStyles}
          >
            {/* //Main View */}
            <View style={styles.headerContainer}>
              <View style={styles.menuButton}>
                <TouchableOpacity onPress={this.openDrawer.bind(this)}>
                  <Image style={{ tintColor: "white" }} source={ic_menu} />
                </TouchableOpacity>
              </View>
              <Text style={styles.headerTitle}>FTL - Shipment Details</Text>
              <View style={styles.menuButton} />
            </View>
            <View style={styles.mainbody}>
              <View style={{ position: "absolute" }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    flexDirection: "column"
                  }}
                >
                  <View
                    style={{
                      paddingVertical: 3,
                      paddingHorizontal: 15,
                      //  borderColor:"#000",
                      //  borderWidth:1,
                      //  borderRadius:1,
                      //  backgroundColor:"#fff",
                      width: (deviceWidth * 2) / 2.1,
                      marginLeft: "2%",
                      flex: 1
                    }}
                  >
                    <View style={{ paddingVertical: 3, flex: 1 }}>
                      <RNPickerSelect
                        placeholder={placeholder_loadType}
                        items={Loads}
                        onValueChange={value => {
                          this.setState({
                            selected_loadType: value
                          });
                        }}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 12
                          },
                          width: 30
                        }}
                        value={this.state.selected_loadType}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Ionicons
                              style={{ paddingTop: 2 }}
                              name="md-arrow-down"
                              size={24}
                              color="#9EA0A4"
                            />
                          );
                        }}
                      />
                    </View>
                    <View style={{ paddingVertical: 10 }}>
                      <RNPickerSelect
                        placeholder={placeholder_direction}
                        items={Loads}
                        onValueChange={value => {
                          this.setState({
                            selected_direction: value
                          });
                        }}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 12
                          }
                        }}
                        value={this.state.selected_direction}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Ionicons
                              style={{ paddingTop: 2 }}
                              name="md-arrow-down"
                              size={24}
                              color="#9EA0A4"
                            />
                          );
                        }}
                      />
                    </View>
                    <View
                      style={{
                        paddingVertical: 10,
                        flex: 1,
                        flexDirection: "row"
                      }}
                    >
                      <TextInput
                        keyboardType={"numeric"}
                        placeholder={"Estimated"}
                        style={{
                          width: "30%",
                          borderWidth: 1,
                          borderColor: "#9EA0A4",
                          borderRadius: 4,
                          backgroundColor: "#fff",
                          paddingHorizontal: 10
                        }}
                      />
                      <RNPickerSelect
                        placeholder={placeholder_estimatedWeight}
                        items={weights}
                        onValueChange={value => {
                          this.setState({
                            selected_estimatedWeight: value
                          });
                        }}
                        style={{
                          ...weightpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 12
                          }
                        }}
                        value={this.state.selected_estimatedWeight}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Ionicons
                              style={{ paddingTop: 2 }}
                              name="md-arrow-down"
                              size={24}
                              color="#9EA0A4"
                            />
                          );
                        }}
                      />
                    </View>
                    <View style={input_description.textarea}>
                      <TextInput
                        multiline={true}
                        placeholder={"Enter Shipment Description"}
                        numberOfLines={4}
                        onChangeText={text => this.setState({ text })}
                        value={this.state.text}
                        style={{
                          height: (deviceHeight * 2) / 14,
                          justifyContent: "flex-start"
                        }}
                      />
                    </View>
                    <View
                      style={{
                        backgroundColor: "#fff",
                        paddingVertical: 10,
                        marginTop: 5
                      }}
                    >
                    <View style={{flex:1, flexDirection:'row', alignItems:'center',justifyContent:'center'}}>
                    <Text Style={{fontSize:18,fontWeight:'bold'}}>* Special Instructions *</Text>
                    </View>
                    
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          paddingHorizontal: 10,
                          paddingVertical: 5
                        }}
                      >
                        <View style={{ flex: 2, flexDirection: "row" }}>
                          <CheckBox
                            style={{ width: 20, height: 20, marginTop: 5 }}
                            value={this.state.loading}
                            onChange={() => this.onChangeLoading()}
                          />
                          <Text style={{ paddingTop: 5, paddingLeft: 10 }}>
                            Loading
                          </Text>
                        </View>

                        <View style={{ flex: 2, flexDirection: "row" }}>
                          <CheckBox
                            style={{ width: 20, height: 20, marginTop: 5 }}
                            value={this.state.craneNeeded}
                            onChange={() => this.onChangeCrane()}

                          />
                          <Text
                            style={{
                              paddingTop: 5,
                              paddingLeft: 10,
                              color: "red"
                            }}
                          >
                            Crane Needed
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          paddingHorizontal: 10,
                          paddingVertical: 5
                        }}
                      >
                        <View style={{ flex: 2, flexDirection: "row" }}>
                          <CheckBox
                            style={{ width: 20, height: 20, marginTop: 5 }}
                            value={this.state.unloading}
                            onChange={() => this.onChangeunloading()}
                          />
                          <Text style={{ paddingTop: 5, paddingLeft: 10 }}>
                            Unloading
                          </Text>
                        </View>

                        <View style={{ flex: 2, flexDirection: "row" }}>
                          <CheckBox
                            style={{ width: 20, height: 20, marginTop: 5 }}
                            value={this.state.containerized}
                            onChange={() => this.onChangeContainerized()}
                          />
                          <Text
                            style={{
                              paddingTop: 5,
                              paddingLeft: 10,
                              color: "red"
                            }}
                          >
                            Containerized
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          paddingHorizontal: 10,
                          paddingVertical: 5
                        }}
                      >
                        <View style={{ flex: 2, flexDirection: "row" }}>
                          <CheckBox
                            style={{ width: 20, height: 20, marginTop: 5 }}
                            value={this.state.heavyLifting}
                            onChange={() => this.onChangeheavyLifting()}
                          />
                          <Text
                            style={{
                              paddingTop: 5,
                              paddingLeft: 10,
                              color: "red"
                            }}
                          >
                            Heavy Lifting
                          </Text>
                        </View>

                        <View style={{ flex: 2, flexDirection: "row" }}>
                          <CheckBox
                            style={{ width: 20, height: 20, marginTop: 5 }}
                            value={this.state.labor}
                            onChange={() => this.onChangeLabor()}

                          />
                          <Text
                            style={{
                              paddingTop: 5,
                              paddingLeft: 10,
                              color: "red"
                            }}
                          >
                            Labor
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    flexDirection: "column"
                  }}
                >
                  <View
                    style={{
                      marginTop: "50%",
                      flex: 1,
                      flexDirection: "row",
                      width: deviceWidth,
                      justifyContent: "center"
                    }}
                  >
                    
                  </View>
                </View>
              </View>
              <View style={{ position: "absolute", bottom: 0 }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignContent: "center",
                    height: 50
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      width: deviceWidth,
                      justifyContent: "center"
                    }}
                  >
                    <Button
                      bordered
                      heightPercentageToDP={50}
                      onPress={() =>
                        this.props.navigation.navigate("UserAccount")
                      }
                      style={{
                        width: (deviceWidth * 2) / 4.2,
                        // height: (deviceHeight * 1.2) / 4 - 120,
                        alignSelf: "flex-end",
                        alignItems: "center",
                        justifyContent: "center",
                        borderColor: "#000",
                        backgroundColor: "#9EA0A4"
                      }}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          color: "#fff",
                          fontSize: 17,
                          fontWeight: "600"
                        }}
                      >
                        Back
                      </Text>
                    </Button>
                    <Button
                      bordered
                      heightPercentageToDP={50}
                      onPress={() =>
                        this.props.navigation.navigate("FTL_SelectTruck")
                      }
                      style={{
                        width: (deviceWidth * 2) / 4.2,
                        // height: (deviceHeight * 1.2) / 4 - 120,
                        alignSelf: "flex-end",
                        alignItems: "center",
                        justifyContent: "center",
                        borderColor: "#000",
                        backgroundColor: "#1c7619",
                        marginLeft: 3
                      }}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          color: "#fff",
                          fontSize: 17,
                          fontWeight: "600"
                        }}
                      >
                        Next
                      </Text>
                    </Button>
                  </View>
                </View>
              </View>
            </View>
          </Drawer>
        </View>
      </SafeAreaView>
    );
  }
}

const drawerStyles = {
  drawer: {
    flex: 1.0,
    backgroundColor: "#3B5998"
  },
  main: {
    flex: 1.0,
    backgroundColor: "white"
  }
};

const styles = {
  mainbody: {
    flex: 1,
    justifyContect: "center"
  },
  mainContainer: {
    flex: 1.0,
    backgroundColor: "white"
  },
  safeAreaStyle: {
    flex: 1.0,
    backgroundColor: "#3B5998"
  },
  headerContainer: {
    height: 44,
    flexDirection: "row",
    justifyContect: "center",
    backgroundColor: "#3B5998"
  },
  headerTitle: {
    flex: 1.0,
    textAlign: "center",
    alignSelf: "center",
    color: "white"
  },
  menuButton: {
    marginLeft: 8,
    marginRight: 8,
    alignSelf: "center",
    tintColor: "white"
  },
  menuContainer: {
    flex: 1.0,
    backgroundColor: "#3B5998"
  },
  menuTitleContainer: {
    alignItem: "center",
    height: 60,
    width: "100%",
    flexDirection: "row"
  },
  menuTitle: {
    width: "100%",
    color: "white",
    textAlign: "center",
    fontSize: 17,
    alignSelf: "center"
  },
  profileImg: {
    height: 100,
    width: 100,
    borderRadius: 50
  },
  drawerMenuHeader: {
    flex: 1,
    alignItems: "center",
    borderBottomWidth: 2
  },
  map: {
    height: (deviceHeight * 2) / 2
  },
  container: {
    paddingVertical: 40,
    paddingHorizontal: 10,
    flex: 1
  }
};


const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 7,
    borderWidth: 1,
    borderColor: "#9EA0A4",
    borderRadius: 4,
    color: "black",
    paddingRight: 30,
    backgroundColor: "#fff",
    width: (deviceWidth * 2) / 2.3
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#9EA0A4",
    borderRadius: 8,
    color: "black",
    paddingRight: 30,
    backgroundColor: "#fff",
    width: (deviceWidth * 2) / 2.3 // to ensure the text is never behind the icon
  }
});
const weightpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 7,
    borderWidth: 1,
    borderColor: "#9EA0A4",
    borderRadius: 4,
    color: "black",
    paddingRight: 30,
    backgroundColor: "#fff",
    width: "100%"
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingLeft: 20,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#9EA0A4",
    borderRadius: 4,
    color: "black",
    paddingRight: 30,
    backgroundColor: "#fff",
    width: "100%",
    paddingRight: 30 // to ensure the text is never behind the icon
  }
});
const input_description = {
  textarea: {
    fontSize: 16,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "#9EA0A4",
    borderRadius: 4,
    color: "black",
    paddingRight: 30,
    backgroundColor: "#fff",
    width: (deviceWidth * 2) / 2.3
  }
};
export default FTL_cargoDescription;
