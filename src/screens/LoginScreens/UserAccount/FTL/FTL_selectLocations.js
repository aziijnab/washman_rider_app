import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  SafeAreaView,
  Dimensions,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import { Button } from "native-base";
import { LinearGradient } from "expo";
import ic_menu from "../Image/list.png";
import Drawer from "react-native-drawer";
console.disableYellowBox = true;
import MapView from "react-native-maps";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import RNPickerSelect from "react-native-picker-select";
import { Ionicons } from "@expo/vector-icons";
import { TextField } from "react-native-material-textfield";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const menu = [
  { title: "Your Orders" },
  { title: "Settings" },
  { title: "Notifications" },
  { title: "Contact us" }
];

const deliveryloc = [
  {
    label: "Express",
    value: "express"
  },
  {
    label: "Normal",
    value: "normal"
  }
];
const pickuploc = [
  {
    label: "Express",
    value: "express"
  },
  {
    label: "Normal",
    value: "normal"
  }
];

const consignees = [
  {
    label: "Express",
    value: "express"
  },
  {
    label: "Normal",
    value: "normal"
  }
];
const placeholder_pickuploc = {
  label: "Select Pickup Location...",
  value: null,
  color: "#9EA0A4"
};
const placeholder_deliveryloc = {
  label: "Select Delivery Location...",
  value: null,
  color: "#9EA0A4"
};
const placeholder_consignee = {
  label: "Select Consignee...",
  value: null,
  color: "#9EA0A4"
};
class FTL_selectLocations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pickup: "",
      dropoff: "",
      selected_consignee: "Select a Delivery Type...",
      selected_deliveryTypes: "Select a Delivery Type...",
      selected_pickuploc: "Select a Pickup Location...",
      selected_deliveryloc: "Select a Pickup Location...",
      radio_value: 0,
      text: "0",
      
    };
  }

  renderDrawer() {
    //SlideMenu
    return (
      <View style={styles.menuContainer}>
        <LinearGradient
          colors={["#ff9966", "#ff5e62"]}
          style={styles.drawerMenuHeader}
        >
          <View style={{ marginVertical: 35 }}>
            <Image
              source={{
                uri:
                  "https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png"
              }}
              style={styles.profileImg}
            />
          </View>
          <View style={{ marginVertical: 10 }}>
            <Text style={{ color: "#fff", fontSize: 22 }}>
              Packages Limited
            </Text>
          </View>
        </LinearGradient>
        <FlatList
          style={{ flex: 1.0 }}
          data={menu}
          extraData={this.state}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity style={styles.menuTitleContainer}>
                <Text style={styles.menuTitle} key={index}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    );
  }

  openDrawer() {
    this.drawer.open();
  }

  closeDrawer() {
    this.drawer.close();
  }
  onSelect(index) {
    this.setState({
      text: `${index}`
    });
  }

  render() {
    const navigation = this.props.navigation;
    const { SignUpEmail } = this.state;
    return (
      <SafeAreaView style={styles.safeAreaStyle}>
        <View style={styles.mainContainer}>
          <Drawer
            ref={ref => (this.drawer = ref)}
            content={this.renderDrawer()}
            type="static"
            tapToClose={true}
            openDrawerOffset={0.35}
            styles={drawerStyles}
          >
            {/* //Main View */}
            <View style={styles.headerContainer}>
              <View style={styles.menuButton}>
                <TouchableOpacity onPress={this.openDrawer.bind(this)}>
                  <Image style={{ tintColor: "white" }} source={ic_menu} />
                </TouchableOpacity>
              </View>
              <Text style={styles.headerTitle}>FTL - Select Locations</Text>
              <View style={styles.menuButton} />
            </View>
            <View style={styles.mainbody}>
              {this.state.text === "0" ? (
                <MapView
                  style={styles.map}
                  region={{
                    latitude: 31.4831569,
                    longitude: 74.1943052,
                    latitudeDelta: 0.1,
                    longitudeDelta: 0.1
                  }}
                ></MapView>
              ) : (
                <View />
              )}

              <View style={{ position: "absolute" }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                   
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor:'#fff'
                    }}
                  >
                    <RadioGroup style={{flex:1, flexDirection:'row', alignItem:'center', justifyContent:'center'}} onSelect={index => this.onSelect(index)} selectedIndex={0}>
                      <RadioButton selected value={"item1"}>
                        <Text>From Map</Text>
                      </RadioButton>

                      <RadioButton value={"item2"}>
                        <Text>From Database</Text>
                      </RadioButton>
                    </RadioGroup>
                  </View>

                  {this.state.text === "1" ? 
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        paddingTop:'30%'
                      }}
                    >
                      <RNPickerSelect
                        placeholder={placeholder_pickuploc}
                        items={pickuploc}
                        onValueChange={value => {
                          this.setState({
                            selected_pickuploc: value
                          });
                        }}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 12
                          }
                        }}
                        value={this.state.selected_pickuploc}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Ionicons
                              style={{ paddingTop: 2 }}
                              name="md-arrow-down"
                              size={24}
                              color="#9EA0A4"
                            />
                          );
                        }}
                      />
                    </View>
                   : 
                    <View></View>
                  }

                  {this.state.text === "1" ? 
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        paddingTop: "4%"
                      }}
                    >
                      <RNPickerSelect
                        placeholder={placeholder_deliveryloc}
                        items={deliveryloc}
                        onValueChange={value => {
                          this.setState({
                            selected_deliveryloc: value
                          });
                        }}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 12
                          }
                        }}
                        value={this.state.selected_deliveryloc}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Ionicons
                              style={{ paddingTop: 2 }}
                              name="md-arrow-down"
                              size={24}
                              color="#9EA0A4"
                            />
                          );
                        }}
                      />
                    </View>
                   : 
                    <View />
                  }

                  {this.state.text === "1" ? (
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        paddingTop: "4%"
                      }}
                    >
                      <RNPickerSelect
                        placeholder={placeholder_consignee}
                        items={consignees}
                        onValueChange={value => {
                          this.setState({
                            selected_deliveryTypes: value
                          });
                        }}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 12
                          }
                        }}
                        value={this.state.selected_consignee}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Ionicons
                              style={{ paddingTop: 2 }}
                              name="md-arrow-down"
                              size={24}
                              color="#9EA0A4"
                            />
                          );
                        }}
                      />
                    </View>
                  ) : (
                    <View></View>
                  )}

                 
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    flexDirection: "column"
                  }}
                >
                  <View
                    style={{
                      marginTop: "50%",
                      flex: 1,
                      flexDirection: "row",
                      width: deviceWidth,
                      justifyContent: "center"
                    }}
                  />
                </View>
              </View>

              <View style={{ position: "absolute", bottom: 0 }}>
                <KeyboardAvoidingView behavior="padding">
                  {this.state.text === "0" ? (
                    <View>
                    <View
                        style={{
                          flex: 1,
                          justifyContent: "center",
                          alignContent: "center",
                          height: 50
                        }}
                      >
                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            width: deviceWidth,
                            justifyContent: "center",
                            marginBottom: (deviceHeight * 1) / 6
                          }}
                        >
                          <TextInput
                          
                            style={{
                              paddingHorizontal: 30,
                              borderColor: "#9EA0A4",
                              borderRadius: 5,
                              borderWidth: 1,
                              height: (deviceHeight * 2) / 25,
                              width: (deviceWidth * 2) / 2.1,
                              backgroundColor: "#fff"
                            }}
                            onChangeText={pickup => this.setState({ pickup })}
                            value={this.state.pickup}
                            placeholder={"Pickup Location"}
                          />
                        </View>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          justifyContent: "center",
                          alignContent: "center",
                          height: 50
                        }}
                      >
                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            width: deviceWidth,
                            justifyContent: "center",
                            marginBottom: (deviceHeight * 1) / 9
                          }}
                        >
                          <TextInput
                            style={{
                              paddingHorizontal: 30,
                              borderColor: "#9EA0A4",
                              borderRadius: 5,
                              borderWidth: 1,
                              height: (deviceHeight * 2) / 25,
                              width: (deviceWidth * 2) / 2.1,
                              backgroundColor: "#fff"
                            }}
                            onChangeText={dropoff => this.setState({ dropoff })}
                            value={this.state.dropoff}
                            placeholder={"Delivery Location"}
                          />
                        </View>
                      </View>
                    </View>
                  ) : (
                    <View />
                  )}

                 
                  </KeyboardAvoidingView>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      alignContent: "center",
                      height: 50
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        width: deviceWidth,
                        justifyContent: "center"
                      }}
                    >
                      <Button
                        bordered
                        heightPercentageToDP={50}
                        onPress={() =>
                          this.props.navigation.navigate("FTL_dateRequired")
                        }
                        style={{
                          width: (deviceWidth * 2) / 4.2,
                          // height: (deviceHeight * 1.2) / 4 - 120,
                          alignSelf: "flex-end",
                          alignItems: "center",
                          justifyContent: "center",
                          borderColor: "#000",
                          backgroundColor: "#9EA0A4"
                        }}
                      >
                        <Text
                          style={{
                            textAlign: "center",
                            color: "#fff",
                            fontSize: 17,
                            fontWeight: "600"
                          }}
                        >
                          Back
                        </Text>
                      </Button>
                      <Button
                        bordered
                        heightPercentageToDP={50}
                        onPress={() =>
                          this.props.navigation.navigate("FTL_paymentMethod")
                        }
                        style={{
                          width: (deviceWidth * 2) / 4.2,
                          // height: (deviceHeight * 1.2) / 4 - 120,
                          alignSelf: "flex-end",
                          alignItems: "center",
                          justifyContent: "center",
                          borderColor: "#000",
                          backgroundColor: "#1c7619",
                          marginLeft: 3
                        }}
                      >
                        <Text
                          style={{
                            textAlign: "center",
                            color: "#fff",
                            fontSize: 17,
                            fontWeight: "600"
                          }}
                        >
                          Next
                        </Text>
                      </Button>
                    </View>
                  </View>
                
              </View>
            </View>
          </Drawer>
        </View>
      </SafeAreaView>
    );
  }
}

const drawerStyles = {
  drawer: {
    flex: 1.0,
    backgroundColor: "#3B5998"
  },
  main: {
    flex: 1.0,
    backgroundColor: "white"
  }
};
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 7,
    borderWidth: 1,
    borderColor: "#9EA0A4",
    borderRadius: 4,
    color: "black",
    paddingRight: 30,
    backgroundColor: "#fff",
    width: (deviceWidth * 2) / 2.3
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: "#9EA0A4",
    borderRadius: 4,
    color: "black",
    paddingRight: 30,
    backgroundColor: "#fff",
    width: (deviceWidth * 2) / 2.3
  }
});
const styles = {
  mainbody: {
    flex: 1,
    justifyContect: "center"
  },
  mainContainer: {
    flex: 1.0,
    backgroundColor: "white"
  },
  safeAreaStyle: {
    flex: 1.0,
    backgroundColor: "#3B5998"
  },
  headerContainer: {
    height: 44,
    flexDirection: "row",
    justifyContect: "center",
    backgroundColor: "#3B5998"
  },
  headerTitle: {
    flex: 1.0,
    textAlign: "center",
    alignSelf: "center",
    color: "white"
  },
  menuButton: {
    marginLeft: 8,
    marginRight: 8,
    alignSelf: "center",
    tintColor: "white"
  },
  menuContainer: {
    flex: 1.0,
    backgroundColor: "#3B5998"
  },
  menuTitleContainer: {
    alignItem: "center",
    height: 60,
    width: "100%",
    flexDirection: "row"
  },
  menuTitle: {
    width: "100%",
    color: "white",
    textAlign: "center",
    fontSize: 17,
    alignSelf: "center"
  },
  profileImg: {
    height: 100,
    width: 100,
    borderRadius: 50
  },
  drawerMenuHeader: {
    flex: 1,
    alignItems: "center",
    borderBottomWidth: 2
  },
  map: {
    height: (deviceHeight * 2) / 2
  }
};
export default FTL_selectLocations;
