//import React from 'react';
import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions,ImageBackground,Image, ActivityIndicator,Alert} from "react-native";
import { LinearGradient } from "expo";
import Constants from 'expo-constants'
import { Button, Content, Container } from "native-base";
import BaseHeader from "../../../components/BaseHeader";
import { TextField } from "react-native-material-textfield";
import { URL } from "../../../components/Api";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;


class RecoveryCode extends Component {
  constructor(props){
    super(props)
    this.state = {
      code:'',
      code_err:'',
      isLoading:false,
    };
    this.navigate = this.props.navigation.navigate;
    this.data = this.props.navigation.state.params;
  }
submit = () => {
  const {code} = this.state;
  if (code == "") {
    this.setState({
      code_err:"Required",
    });
  }
  if (code != "") {
    if (code.length != 6) {
      this.setState({ code_err: "must be at least 6 digits" });
    } else {
      this.setState({ code_err: "" });
    }
  }
  if(code != "" && code.length == 6){
    this.setState({isLoading:true});
    fetch(URL + "recover-password", {
      method: "POST",
      body: JSON.stringify({
        email: this.data.email,
        code: code,
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        if (response.response == "success") {
          this.setState({ isLoading: false });
         this.props.navigation.navigate('RecoverPassword',{"email":this.data.email})
        } 
         else {
          Alert.alert(
            'Sorry',
            response.message,
            [
              {text: 'OK'},
            ],
            {cancelable: true},
          );
          this.setState({ isLoading: false });
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  }
}

  render() {
    
    return (
      
      <View style={{height:deviceHeight, width:deviceWidth}}>

 <ImageBackground source={require('../../../../assets/bg.png')} style={{ height: deviceHeight, width:deviceWidth, resizeMode: "contain"}}>
 <BaseHeader
          PageTitle=""
          IconLeft="ios-arrow-back"
          drawerOpen="LoginHome"
          navigation={this.props.navigation}
        />
 <View
            style={{
              alignItems: "center",
              paddingTop:"10%",
            }}
          >
             <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 26, fontWeight: "800", color: "#fff" }}>
               Verification Code
              </Text>
             </View>
             <View
                style={{
                  width: (deviceWidth * 2) / 2.5,
                  paddingBottom:100,
                  paddingTop:"30%"
                }}
              >
                <TextField
                  autoCapitalize="none"
                  error={this.state.code_err}
                  onChangeText={code=>this.setState({code})}
                  autoCorrect={false}
                  label="Verification Code"
                  maxLength={6}
                  tintColor={"#fff"}
                  baseColor="#fff"
                  textColor="#fff"
                />
                
               
                <View style={{flexDirection:"row",marginTop:"10%", alignItems:"center", alignContent:"center", justifyContent:"center"}}>
               {this.state.isLoading ? (
                <Button
            bordered
            heightPercentageToDP={50}
            style={{
              width: (deviceWidth * 2) / 4,
              height:50,
              alignItems: "center",
              justifyContent: "center",
              marginTop: 15,
              borderColor: "#DA2128",
              backgroundColor: "#DA2128",
              borderRadius:25
            }}
          >
          <ActivityIndicator size="small" />
          </Button>
               ):(
                <Button
                onPress={this.submit}
            bordered
            heightPercentageToDP={50}
            style={{
              width: (deviceWidth * 2) / 4,
              height:50,
              alignItems: "center",
              justifyContent: "center",
              marginTop: 15,
              borderColor: "#DA2128",
              backgroundColor: "#DA2128",
              borderRadius:25
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#fff",
                fontSize: 17,
                fontWeight: "600"
              }}
            >
                Submit
            </Text>
          </Button>
               )}
                
                </View>
                <View style={{paddingVertical:20,flexDirection:"row", alignItems:"center", alignContent:"center", justifyContent:"center"}}>
                <Image source={require('../../../../assets/logofade.png')} style={{resizeMode: "contain", height:90, width:90}}>

                </Image>
                </View>
                
              </View>
              
            
              {/* ............Button................. */}
          </View>
        

</ImageBackground>
      </View>
     
     );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default RecoveryCode;
