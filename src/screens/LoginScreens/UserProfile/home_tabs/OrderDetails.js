//import React from 'react';

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
   Alert,
  ActivityIndicator
} from "react-native";
import { Google } from 'expo';
import * as Facebook from 'expo-facebook';
import Constants from 'expo-constants'
import { Button, Content } from "native-base";
import BaseHeader from "../../../../components/BaseHeader";
import { URL, TOKEN } from "../../../../components/Api";
import AccountHeader from "../../../../components/AccountHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class OrderDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delivery_type:"normal",
      CustomerID: null,
      email: "",
      address:"",
      isNormal:false,
      lat:null,
      lng:null,
      note:"",
      urgent_disabled:false,
      normal_disabled: true,
      order_details:[],
      item_ids:[],
      item_quantities:[],
      total_price:null,
      discount_price:null,
      total_items:null,
    };
    this.data = this.props.navigation.state.params;
    console.log(this.data.order_details);
  }

  render() {
    const { navigation } = this.props.navigation;
    
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
      <AccountHeader navigation={this.props.navigation} back={true} />
<Content>
       <View
            style={{
              alignItems: "center",
              paddingTop: "15%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}>
              ORDER DETAILS
              </Text>

             </View>
            <View style={{width:deviceWidth}}>
            <View style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:30,fontFamily:'neuron_bold',}}>
                 Items
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:30,fontFamily:'neuron_bold',}}>
                 Quantity
                </Text>
              </View>
              </View>
              { 
                    this.data.order_details.map((item, index)=>{
                        return (
                          <View key={index} style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                 {item.Name}
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                 {item.Quantity}
                </Text>
              </View>
              </View>
                        )
                    })
              }
              <View style={{flexDirection:'row',marginTop:50,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:30,fontFamily:'neuron_bold',}}>
                 Bill
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                <Text style={{fontSize:25,fontFamily:'neuron_bold'}}>Rs: </Text> {this.data.bill}
                </Text>
              </View>
              </View>
             </View>
            {/* ............Button................. */}
          </View>
          </Content>
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default OrderDetails;
