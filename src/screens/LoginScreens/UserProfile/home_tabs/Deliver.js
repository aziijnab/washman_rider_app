//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Platform,
  ToastAndroid,
  Alert
} from "react-native";
import {
  Text,
  Button,
  Icon,
  Container
} from "native-base";
import { URL, TOKEN } from "../../../../components/Api";
import { CheckBox } from 'react-native-elements'
import AccountHeader from "../../../../components/AccountHeader";
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import MapView, {Marker, Polyline} from 'react-native-maps';
import PolyLine from "@mapbox/polyline";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Deliver extends Component {
  constructor(props) {
    super(props);
    this.state = {
        location: null,
        errorMessage: null,
        cash_receive:false,
        destination_lat: "31.460089",
        destination_lng: "74.321933",
        lat: null,
        long: null,
        status: false,
        directions: [],
    };
    this.navigate = this.props.navigation.navigate;
    this.data = this.props.navigation.state.params;
    console.log(this.data)
  }
  cashReceived = () => {
    if(!this.state.cash_receive){
      fetch(URL + "Order/UpdateOrderPaymentReceived", {
        method: "PUT",
        headers: {
         AuthToken:TOKEN,
         OrderID: this.data.order_id,
        }
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.ResponseCode == 200) {
            ToastAndroid.show("Cash Received",3000);
            this.setState({ cash_receive: !this.state.cash_receive })
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }
        })
        .catch(error => alert("Please check internet connection"));
    }
  }
  updateDelivered = () => {
    if(!this.data.package){

      if(!this.state.cash_receive){
        ToastAndroid.show("Cash has not been received yet",3000);
      }
      else{
        fetch(URL + "Order/UpdateOrderDelivered", {
          method: "PUT",
          headers: {
           AuthToken:TOKEN,
           OrderID: this.data.order_id,
          }
        })
          .then(res => res.json())
          .then(async response => {
            console.log(response);
            if (response.ResponseCode == 200) {
              ToastAndroid.show("Order has been delivered",3000);
              this.props.navigation.push("HomePage")
            } 
             else {
              Alert.alert(
                'Sorry',
                response.MsgToShow,
                [
                  {text: 'OK'},
                ],
                {cancelable: true},
              );
              this.setState({ isLoading: false });
            }
          })
          .catch(error => alert("Please check internet connection"));
      }
    }
    else{
      fetch(URL + "Order/UpdateOrderDelivered", {
        method: "PUT",
        headers: {
         AuthToken:TOKEN,
         OrderID: this.data.order_id,
        }
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.ResponseCode == 200) {
            ToastAndroid.show("Order has been delivered",3000);
            this.props.navigation.push("HomePage")
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }
        })
        .catch(error => alert("Please check internet connection"));
    }
    
  }
  
  componentWillMount() {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
      

    }
  }
  // _getDirections = async (lat,long) => {
  _getDirections = async (lat,lng) => {
  const responses = await fetch(
      `https://maps.googleapis.com/maps/api/directions/json?origin=${lat},${lng}&destination=${parseFloat(this.data.lat)},${parseFloat(this.data.lng)}&key=AIzaSyDxrvONirP_G3B0nBeEsF8LebHY04lVj0Q`
    );

    let json_destinations = await responses.json();
    if (json_destinations.status == "ZERO_RESULTS") {
      this.setState({ status: true });
    } else {
  const points = PolyLine.decode(
    json_destinations.routes[0].overview_polyline.points
  );

  const pointCoordss = points.map(point => {
    return { latitude: point[0], longitude: point[1] };
  });
  this.setState({directions : pointCoordss })
}
}
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    this.setState({ lat: location.coords.latitude,long: location.coords.longitude });
    this.map.animateToCoordinate({
        latitude:location.coords.latitude,
        longitude:location.coords.longitude,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05
    });
    this._getDirections(location.coords.latitude,location.coords.longitude);
  };
  render() {
    let { image } = this.state;
    return (
        <Container>
       <AccountHeader navigation={this.props.navigation} back={true} />
       
             <View>
             <MapView 
              ref={map => {
                    this.map = map;
                  }}
             style={{height:deviceHeight/1.7}}
             showsUserLocation={true}
             initialRegion={this.state.lat ? {
                latitude:this.state.lat,
        longitude:this.state.long,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05
             } : null}
              >
                {this.state.directions ? ( 
                  <Polyline
                    coordinates={this.state.directions}
                    strokeWidth={2}
                    strokeColor="red"
                  />
                   ):null }
                <Marker coordinate={{
            latitude : parseFloat(this.data.lat),
            longitude: parseFloat(this.data.lng)
          }}>
            <Icon name="pin" color="red"/>
          </Marker>
              </MapView>

              <View style={{width:deviceWidth, alignItems:"center", alignContent:"center", paddingTop:10}}>
              <Button
                  onPress={() => this.props.navigation.navigate("OrderDetails",{
                    order_details: this.data.items,
                    bill: this.data.bill,
                  })}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: "90%",
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 22,
                      fontFamily:"neuron_regular"
                    }}
                  >
                    ORDER DETAILS
                  </Text>
                </Button>
              
              </View>
              {this.data.package ? (
                <View style={{width:deviceWidth, alignItems:"center", alignContent:"center", paddingTop:10}}>
              
              <Button
                  onPress={this.updateDelivered}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 22,
                      fontFamily:"neuron_regular"
                    }}
                  >
                    Delivered
                  </Text>
                </Button>
              </View>
            
              ) : (
                <View style={{flexDirection:"row", width:deviceWidth-20, paddingTop:10}}>
               <View style={{width:'50%'}}>
               <CheckBox
  title='Cash Received'
  checked={this.state.cash_receive}
  onPress={this.cashReceived}
/>
              </View>
              <View style={{width:'50%',paddingTop:5,alignItems:"flex-end"}}>
              <Button
                  onPress={this.updateDelivered}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: "70%",
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 22,
                      fontFamily:"neuron_regular"
                    }}
                  >
                    Delivered
                  </Text>
                </Button>
              </View>
              </View>
            
              )}
               </View>
             
          </Container>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default Deliver;
