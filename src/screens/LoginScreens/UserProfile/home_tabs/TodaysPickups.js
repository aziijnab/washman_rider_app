//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import Constants from "expo-constants";
import { URL, TOKEN } from "../../../../components/Api";
import {
  Content,

  Alert,
  Text,
  Button,
  
  Container
} from "native-base";

import AccountHeader from "../../../../components/AccountHeader";
import buttonImg from "../../../../../assets/button.png";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class TodaysPickups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders:[],
      loading:true,
      rider_id:null,
    };
    this.navigate = this.props.navigation.navigate;
  }
 componentWillMount = async () => {
  AsyncStorage.getItem("userID").then(user_data => {
    const val = JSON.parse(user_data);
    if(val){
      this.setState({rider_id : val.RiderID});
    }
  });
  try {
    setInterval(async () => {
      fetch(URL + "Rider/GetTodayPickups", {
        method: "GET",
        headers: {
         AuthToken:TOKEN
        }
      })
        .then(res => res.json())
        .then(async response => {
          if (response.ResponseCode == 200) {
           
            this.setState({ orders: response.list, loading: false });
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }
        })
        .catch(error => alert("Please check internet connection"));
    }, 3000);
  } catch(e) {
    console.log(e);
  }
  
 }
 startOrder = (OrderID, Lat, Long, list, Bill, isPackage) => {
  fetch(URL + "Order/AssociateRider", {
    method: "PUT",
    headers: {
     AuthToken:TOKEN,
     OrderID:OrderID,
     RiderID:this.state.rider_id,
    }
  })
    .then(res => res.json())
    .then(async response => {
      if (response.ResponseCode == 200) {
       
        this.props.navigation.navigate("Pickup",{
          order_id : OrderID,
          lat : Lat,
          lng : Long,
          items : list,
          bill : Bill,
          package : isPackage,
        })
      } 
       else {
        Alert.alert(
          'Sorry',
          response.MsgToShow,
          [
            {text: 'OK'},
          ],
          {cancelable: true},
        );
        this.setState({ isLoading: false });
      }
    })
    .catch(error => alert("Please check internet connection"));

  
 }
  render() {
    return (
        <Container>
       <AccountHeader navigation={this.props.navigation} back={true} />
       <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:40, paddingTop:30}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_bold",textAlign:'center' }}>
               TODAY'S
              </Text>
             </View>
       <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:10}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_bold",textAlign:'center' }}>
               PICKUPS
              </Text>
             </View>
             {!this.state.loading ? (
              <Content contentContainerStyle={{paddingTop:60}}>
              
          <View style={{width:deviceWidth, alignContent:"center", alignItems:"center", paddingBottom:60}}>
          <View style={{width:"90%", flexDirection:"row" , borderWidth:1, borderColor:"#000"}}>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>ID</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>CLIENT NAME</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>ORDER TYPE</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>ACTION</Text>
         </View>
          </View>
          {this.state.orders.map((item, index)=>{
                        return (
                          <View key={index} style={{width:"90%", flexDirection:"row" , borderWidth:0.5, borderColor:"#000"}}>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_regular"}}>{item.OrderID}</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_regular"}}>{item.CustomerName}</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
          
           {item.IsPackage ?  <Text style={{fontFamily:"neuron_regular"}}>Package</Text>  : !item.IsPackage && item.IsPromotion ? <Text style={{fontFamily:"neuron_regular"}}>Promotion</Text> : <Text style={{fontFamily:"neuron_regular"}}>Normal</Text> }
           
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5, alignContent:"center",alignItems:"center"}}>
         <Button
                  onPress={() => this.props.navigation.navigate("DetailsForRider",{
                    order_details: item.list,
                    order: item,
                    bill: item.Bill,
                    lat: item.Lat,
                    lng: item.Long,
                    note: item.Note,
                  })}         
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: "95%",
                    height:25,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 12,
                      fontFamily:"neuron_regular",
                      paddingHorizontal:5
                    }}
                  >
                    DETAILS
                  </Text>
                </Button>
         </View>
          </View>  
                        )
                        })}
         
          </View>
          </Content>
         
             ) : (
             

<View style={{width:deviceWidth, alignContent:"center", alignItems:"center", marginTop:50}}>
               <View style={{width:"60%"}}>
<ActivityIndicator size={"large"} color={"#000"} />
               </View>

               </View>
             )}
          </Container>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#808080",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
  colorWhite: {
    color: "#fff"
  },
});

export default TodaysPickups;
