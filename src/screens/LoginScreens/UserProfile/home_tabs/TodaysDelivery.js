//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ActivityIndicator
} from "react-native";
import Constants from "expo-constants";
import { URL, TOKEN } from "../../../../components/Api";
import {
  Content,
  Card,
  CardItem,
  Thumbnail,
  Alert,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Textarea,
  Form,
  Footer, FooterTab,
  Container
} from "native-base";

import AccountHeader from "../../../../components/AccountHeader";
import buttonImg from "../../../../../assets/button.png";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class TodaysDelivery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders:[],
      loading:true,
    };
    this.navigate = this.props.navigation.navigate;
  }
 componentWillMount = async () => {
  try {
    setInterval(async () => {
      fetch(URL + "Rider/GetTodayDeliveries", {
        method: "GET",
        headers: {
         AuthToken:TOKEN
        }
      })
        .then(res => res.json())
        .then(async response => {
          if (response.ResponseCode == 200) {
            this.setState({ orders: response.list, loading:false });
            console.log(this.state.orders.length)
          } 
           else {
            Alert.alert(
              'Sorry',
              response.MsgToShow,
              [
                {text: 'OK'},
              ],
              {cancelable: true},
            );
            this.setState({ isLoading: false });
          }
        })
        .catch(error => alert("Please check internet connection"));
    }, 3000);
  } catch(e) {
    console.log(e);
  }
  
 }
  render() {
    return (
        <Container>
       <AccountHeader navigation={this.props.navigation} back={true} />
       <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:40, paddingTop:30}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_bold",textAlign:'center' }}>
               TODAY'S
              </Text>
             </View>
       <View style={{width:deviceWidth, alignItems:'center', alignContent:"center",paddingHorizontal:10}}>
             
             <Text style={{ fontSize: 35, color: "#000", fontFamily:"neuron_bold",textAlign:'center' }}>
               DELIVERIES
              </Text>
             </View>
             {this.state.loading ? (
              <View style={{width:deviceWidth, alignContent:"center", alignItems:"center", marginTop:50}}>
               <View style={{width:"60%"}}>
<ActivityIndicator size={"large"} color={"#000"} />
               </View>

               </View>
             ) : this.state.orders.length > 0 ? (
              <Content contentContainerStyle={{paddingTop:60}}>
          <View style={{width:deviceWidth, alignContent:"center", alignItems:"center", paddingBottom:60}}>
          <View style={{width:"90%", flexDirection:"row" , borderWidth:1, borderColor:"#000"}}>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>ID</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>CLIENT NAME</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>ORDER TYPE</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_bold"}}>NAVIGATION</Text>
         </View>
          </View>
          {this.state.orders.map((item, index)=>{
                        return (
                          <View key={index} style={{width:"90%", flexDirection:"row" , borderWidth:0.5, borderColor:"#000"}}>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_regular"}}>{item.OrderID}</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
           <Text style={{fontFamily:"neuron_regular"}}>{item.CustomerName}</Text>
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5}}>
          
           {item.IsPackage ?  <Text style={{fontFamily:"neuron_regular"}}>Package</Text>  : !item.IsPackage && item.IsPromotion ? <Text style={{fontFamily:"neuron_regular"}}>Promotion</Text> : <Text style={{fontFamily:"neuron_regular"}}>Normal</Text> }
           
         </View>
         <View style={{width:"25%", borderWidth:0.5, borderColor:"#000", padding:5, alignContent:"center",alignItems:"center"}}>
         <Button
                  // onPress={this.submit}
                  onPress={() => this.props.navigation.navigate("Deliver",{
                    order_id : item.OrderID,
                    lat : item.Lat,
                    lng : item.Long,
                    items : item.list,
                    bill : item.Bill,
                    package : item.IsPackage,
                  })}
                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: "95%",
                    height:25,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 8,
                      fontFamily:"neuron_regular",
                      paddingHorizontal:5
                    }}
                  >
                    DIRECTION
                  </Text>
                </Button>
         </View>
          </View>  
                        )
                        })}
         
          </View>
          </Content>
         
             ) : (
              <View style={{width:deviceWidth, alignContent:"center", alignItems:"center", marginTop:100}}>
               <View style={{width:"90%"}}>
               <Text style={{fontSize:20, fontFamily:"neuron_regular", color:"#808080"}}>You have no Orders to Deliver yet</Text>
               </View>

               </View>
             ) }
          </Container>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  popup: {
    backgroundColor: "#808080",
    marginTop: 80,
    marginHorizontal: 20,
    borderRadius: 7
  },
  popupOverlay: {
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    //alignItems: 'center',
    backgroundColor: "#eff0f1",
    margin: 5
  },
  popupHeader: {
    marginBottom: 45
  },
  colorWhite: {
    color: "#fff"
  },
});

export default TodaysDelivery;
