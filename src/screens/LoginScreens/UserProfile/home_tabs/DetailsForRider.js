//import React from 'react';

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import { URL, TOKEN } from "../../../../components/Api";
import MapView, {Marker, Polyline} from 'react-native-maps';
import PolyLine from "@mapbox/polyline";
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { Content , Icon,Alert,
  Text,
  Button} from "native-base";
import AccountHeader from "../../../../components/AccountHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class DetailsForRider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delivery_type:"normal",
      rider_id:null,
      CustomerID: null,
      email: "",
      address:"",
      isNormal:false,
      lat:null,
      lng:null,
      note:"",
      urgent_disabled:false,
      normal_disabled: true,
      order_details:[],
      item_ids:[],
      item_quantities:[],
      total_price:null,
      discount_price:null,
      total_items:null,

      location: null,
       
        destination_lat: "31.460089",
        destination_lng: "74.321933",
        lat: null,
        long: null,
        status: false,
        directions: [],
    };
    this.data = this.props.navigation.state.params;
    console.log(this.data);
  }
  componentWillMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if(val){
        this.setState({rider_id : val.RiderID});
      }
    });
      this._getLocationAsync();
      

  }
  startOrder = (OrderID, Lat, Long, list, Bill, isPackage) => {
    
    fetch(URL + "Order/AssociateRider", {
      method: "PUT",
      headers: {
       AuthToken:TOKEN,
       OrderID:OrderID,
       RiderID:this.state.rider_id,
      }
    })
      .then(res => res.json())
      .then(async response => {
        if (response.ResponseCode == 200) {
         
          this.props.navigation.navigate("Pickup",{
            order_id : OrderID,
            lat : Lat,
            lng : Long,
            items : list,
            bill : Bill,
            package : isPackage,
          })
        } 
         else {
          Alert.alert(
            'Sorry',
            response.MsgToShow,
            [
              {text: 'OK'},
            ],
            {cancelable: true},
          );
          this.setState({ isLoading: false });
        }
      })
      .catch(error => alert("Please check internet connection"));
  
    
   }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    this.setState({ lat: location.coords.latitude,long: location.coords.longitude });
    this.map.animateToCoordinate({
        latitude:location.coords.latitude,
        longitude:location.coords.longitude,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05
    });
    this._getDirections(location.coords.latitude,location.coords.longitude);
  };
  _getDirections = async (lat,lng) => {
    
    const responses = await fetch(
        `https://maps.googleapis.com/maps/api/directions/json?origin=${lat},${lng}&destination=${parseFloat(this.data.lat)},${parseFloat(this.data.lng)}&key=AIzaSyDxrvONirP_G3B0nBeEsF8LebHY04lVj0Q`
      );
  
      let json_destinations = await responses.json();
      if (json_destinations.status == "ZERO_RESULTS") {
        this.setState({ status: true });
      } else {
    const points = PolyLine.decode(
      json_destinations.routes[0].overview_polyline.points
    );
  
    const pointCoordss = points.map(point => {
      return { latitude: point[0], longitude: point[1] };
    });
    this.setState({directions : pointCoordss })
  }
  }
  render() {
    const { navigation } = this.props.navigation;
    
    return (
      <View style={{ width: deviceWidth, height:deviceHeight }}>
      <AccountHeader navigation={this.props.navigation} back={true} />

       <View
            style={{
              alignItems: "center",
              paddingTop: "5%"
            }}
          >
            <View style={{width:deviceWidth, alignItems:'center', alignContent:"center"}}>
             <Text style={{ fontSize: 50, color: "#000", fontFamily:"neuron_bold" }}>
              ORDER DETAILS
              </Text>

             </View>
            <View style={{width:deviceWidth, height:(deviceHeight / 2) - 130}}>
            <MapView 
              ref={map => {
                    this.map = map;
                  }}
             style={{height:(deviceHeight / 2) - 130}}
             showsUserLocation={true}
             initialRegion={this.state.lat ? {
                latitude:this.state.lat,
        longitude:this.state.long,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05
             } : null}
              >
                {this.state.directions ? ( 
                  <Polyline
                    coordinates={this.state.directions}
                    strokeWidth={2}
                    strokeColor="red"
                  />
                   ):null }
                <Marker coordinate={{
            latitude : parseFloat(this.data.lat),
            longitude: parseFloat(this.data.lng)
          }}>
            <Icon name="pin" color="red"/>
          </Marker>
              </MapView>

             </View>
             
             
          </View>
          <Content> 
          <View style={{width:deviceWidth, paddingBottom:100}}>
            <View style={{flexDirection:'row',marginTop:20}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:30,fontFamily:'neuron_bold',}}>
                 Items
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:30,fontFamily:'neuron_bold',}}>
                 Quantity
                </Text>
              </View>
              </View>
              { 
                    this.data.order_details.map((item, index)=>{
                        return (
                          <View key={index} style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:22,fontFamily:'neuron_regular',}}>
                 {item.Name}
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:22,fontFamily:'neuron_regular',}}>
                 {item.Quantity}
                </Text>
              </View>
              </View>
                        )
                    })
              }
            
              
              
              
              <View style={{flexDirection:'row',marginTop:20,}}>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:30,fontFamily:'neuron_bold',}}>
                 Bill
                </Text>
              </View>
              <View style={{width:'50%'}}>
              <Text style={{textAlign:'center',fontSize:25,fontFamily:'neuron_regular',}}>
                <Text style={{fontSize:25,fontFamily:'neuron_bold'}}>Rs: </Text> {this.data.bill}
                </Text>
              </View>
              </View>
              {this.data.note ? (
                <View style={{marginTop:10,width:deviceWidth, alignContent:"center", alignItems:"center"}}>
              <View style={{width:'80%', borderColor:"#808080", borderWidth:0.5, borderRadius:10, padding:10, backgroundColor:"#E7EDF1"}}>
              <Text style={{fontFamily:"neuron_bold", fontSize:16, color:"#000"}}>Note :</Text>
              <Text style={{fontFamily:"neuron_regular", fontSize:14, color:"#000"}}>{this.data.note}</Text>
              </View>
              
              </View>
              ) : null}
             
            
             <View style={{marginTop:30,width:deviceWidth, alignContent:"center", alignItems:"center"}}>
              <View style={{width:'80%'}}>
              <Button
                  onPress={() => this.startOrder(this.data.order.OrderID, this.data.order.Lat, this.data.order.Long, this.data.order_details, this.data.order.Bill, this.data.order.IsPackage)}

                  bordered
                  heightPercentageToDP={50}
                  style={{
                    width: "100%",
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 15,
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#fff",
                      fontSize: 30,
                      fontFamily:"neuron_bold"
                    }}
                  >
                    Start
                  </Text>
                </Button>
              </View>
              
              </View>
            
             </View>
            
            {/* ............Button................. */}
            </Content>
         
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default DetailsForRider;
