//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  Image,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import Constants from 'expo-constants'
import { Button, Content, Container } from "native-base";
import AccountHeader from "../../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.navigate = this.props.navigation.navigate;
  }
  logout = () => {
    AsyncStorage.clear();
    this.props.navigation.push("LoginForm");
  }
  render() {
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
     
     <AccountHeader
           navigation={this.props.navigation}
         />
         
          <View
            style={{
              alignItems: "center",
              alignContent:"center",
              justifyContent:"center",
              height:deviceHeight - 100
            }}
          >
           
            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingTop: 20,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Button
               onPress={() => this.props.navigation.navigate("TodaysPickups")}
                bordered
                heightPercentageToDP={50}
                style={{
                    width: (deviceWidth * 2) / 3,
                    height: 100,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 30,
                    fontFamily:"neuron_regular"
                  }}
                >
                  TODAYS PICKUP'S
                </Text>
              </Button>
            </View>

            <View
              style={{
                width: (deviceWidth * 2) / 2.5,
                paddingTop: 20,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Button
              onPress={() => this.props.navigation.navigate("TodaysDelivery")}
                bordered
                heightPercentageToDP={50}
                style={{
                    width: (deviceWidth * 2) / 3,
                    height: 100,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "#45c8d0",
                    backgroundColor: "#45c8d0",
                    
                  }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 30,
                    fontFamily:"neuron_regular"
                  }}
                >
                  TODAYS DELIVERY
                </Text>
              </Button>
            </View>
            
           
            {/* ............Button................. */}
          </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  }
});

export default HomePage;
