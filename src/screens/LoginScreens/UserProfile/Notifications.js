//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Image,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import Constants from "expo-constants";
import { Content, Card, CardItem, Body, Text } from "native-base";
import AccountHeader from "../../../components/AccountHeader";
import { TextField } from "react-native-material-textfield";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.navigate = this.props.navigation.navigate;
  }
  logout = () => {
    AsyncStorage.clear();
    this.props.navigation.push("LoginForm");
  };
  render() {
    return (
      <View style={{ height: deviceHeight, width: deviceWidth }}>
        <ImageBackground
          source={require("../../../../assets/bg.png")}
          style={{
            height: deviceHeight,
            width: deviceWidth,
            resizeMode: "contain"
          }}
        >
          <AccountHeader back={true} navigation={this.props.navigation} />
          <View
            style={{
              alignItems: "center",
              paddingTop: "5%"
            }}
          >
            <View
              style={{
                width: deviceWidth,
                paddingTop: 20,
                paddingBottom: 20,
                alignItems: "center",
                alignContent: "center"
              }}
            >
              <Text style={{ fontSize: 26, fontWeight: "800", color: "#fff" }}>
                Notifications
              </Text>
            </View>
          </View>
          <Content
            contentContainerStyle={{
              paddingTop: 10,
              paddingBottom: 50,
              width: deviceWidth,
              alignContent: "center",
              alignItems: "center"
            }}
          >
            <Card style={{ width: "95%" }}>
              <CardItem>
                <Body>
                  <View style={{ width: "100%", flexDirection: "row" }}>
                    <View>
                      <Image
                        source={{
                          uri:
                            "https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png"
                        }}
                        style={styles.profileImg}
                      />
                    </View>
                    <View style={{ paddingHorizontal: 20, width: "95%" }}>
                      <Text>You have a connection request from Alex</Text>
                      <Text style={{ fontSize: 12, color: "#808080" }}>
                        24 minutes ago
                      </Text>
                    </View>
                  </View>
                </Body>
              </CardItem>
            </Card>
          </Content>
          <View
            style={{
              alignItems: "center",
              paddingTop: "5%"
            }}
          >
            <View
              style={{
                paddingVertical: 20,
                flexDirection: "row",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center"
              }}
            >
              <Image
                source={require("../../../../assets/logofade.png")}
                style={{ resizeMode: "contain", height: 90, width: 90 }}
              />
            </View>
          </View>

          {/* ............Button................. */}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight
  },
  profileImg: {
    height: 50,
    width: 50,
    borderRadius: 25
  }
});

export default Notifications;
